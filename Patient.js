var mongoose = require('mongoose');

// note: your host/port number may be different!
mongoose.connect('mongodb+srv://nabeel:nabeel@fanyhealth-mek3v.mongodb.net/test?retryWrites=true&w=majority');

var Schema = mongoose.Schema;

var patientSchema = new Schema( {
      username: {type: String, required: true},
      password: {type: String, required: true},
      email: {type: String, required: true},

      telephone: {type: String, required: true}, 
      fullname: {type: String}
    } );


module.exports = mongoose.model('Patient', patientSchema);

