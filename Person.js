var mongoose = require('mongoose');

// note: your host/port number may be different!
mongoose.connect('mongodb+srv://nabeel:nabeel@fanyhealth-mek3v.mongodb.net/test?retryWrites=true&w=majority');

var Schema = mongoose.Schema;

var personSchema = new Schema( {
      name: {type: String, required: true, unique: true},
      age: Number
    } );


module.exports = mongoose.model('Person', personSchema);

