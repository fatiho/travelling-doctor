package edu.upenn.cis350;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.Button;
import android.widget.TableLayout;
import android.widget.TableRow;
import android.widget.TextView;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.net.*;

import androidx.annotation.RequiresApi;
import androidx.appcompat.app.AppCompatActivity;

import java.util.ArrayList;

import static android.provider.ContactsContract.CommonDataKinds.Website.URL;

public class InventoryActivity extends AppCompatActivity {
    public static final int ADD_ITEM_CLICK = 1;
    private ArrayList<String> names = new ArrayList<>();
    private ArrayList<Long> quantities = new ArrayList<>();
    private ArrayList<String> dates = new ArrayList<>();
    boolean fullTableShown = false;
    Context context = null;

    @RequiresApi(api = Build.VERSION_CODES.KITKAT)
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_doctor_inventory);
        try {
            doSomething();
        } catch (IOException e) {
            Log.d("oops: ", String.valueOf(e));
        }
        context = this;
    }


    public void onDetailsClickButton(View view) {
        TableLayout table = findViewById(R.id.table);
        Button detailsButton = findViewById(R.id.detailsButton);
        table.setColumnCollapsed(1, fullTableShown);
        table.setColumnCollapsed(2, fullTableShown);

        if (fullTableShown) {
            fullTableShown = false;
            detailsButton.setText("Show Details");
        } else {
            fullTableShown = true;
            detailsButton.setText("Hide Details");
        }
    }

    public void onAddItemButton(View view) {
        Intent intent = new Intent(this, AddItem.class);
        startActivityForResult(intent, ADD_ITEM_CLICK);
    }

    @RequiresApi(api = Build.VERSION_CODES.KITKAT)
    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (requestCode == ADD_ITEM_CLICK) {
            if (resultCode == Activity.RESULT_OK) {
                try {
                    doSomething();
                } catch (IOException e) {
                    Log.d("oops: ", String.valueOf(e));
                }
                String[] result = data.getStringArrayExtra("Result");

                //Get table
                TableLayout inventory = (TableLayout) findViewById(R.id.table);

                //Get row format
                LayoutInflater inflater = (LayoutInflater)
                        this.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
                TableRow row = (TableRow) inflater.inflate(R.layout.tablerow, null);

                //Get children in row and set values
                TextView name = (TextView) row.getVirtualChildAt(0);
                name.setText(result[0]);
                TextView quantity = (TextView) row.getVirtualChildAt(1);
                quantity.setText(result[1]);
                TextView date = (TextView) row.getVirtualChildAt(2);
                date.setText(result[2]);

                //Add row to table
                inventory.addView(row);
            }

            if (resultCode == Activity.RESULT_CANCELED) {
                //Do nothing here, as of now
            }
        }
    }

    @RequiresApi(api = Build.VERSION_CODES.KITKAT)
    public void doSomething() throws IOException {
        URL url = new URL ("http://10.0.2.2:3000/createItem");
        HttpURLConnection con = (HttpURLConnection)url.openConnection();
        con.setRequestMethod("POST");
        con.setRequestProperty("Content-Type", "application/json; charset=utf-8");
        con.setRequestProperty("Accept", "application/json");
        con.setDoOutput(true);
        String jsonInputString = "{name: Upendra, quantity: 23}";
        try(OutputStream os = con.getOutputStream()) {
            byte[] input = jsonInputString.getBytes("utf-8");
            os.write(input, 0, input.length);

        }

        try(BufferedReader br = new BufferedReader(new InputStreamReader(con.getInputStream(), "utf-8"))) {
            StringBuilder response = new StringBuilder();
            String responseLine = null;
            while ((responseLine = br.readLine()) != null) {
                response.append(responseLine.trim());
            }
            System.out.println(response.toString());
        }

//        try {
//
//            URL url = new URL("http://10.0.2.2:3000/create?name=Wire&quantity=50&lastUpdated=18");
//            AccessWebTask task = new AccessWebTask();
//            task.execute(url);
//            String name = task.get();
//            Log.d("We got this:", name);
//        } catch (Exception e) {
//            Log.d("oops", "problem");
//        }
    }

}
