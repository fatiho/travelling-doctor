package edu.upenn.cis350;

public class Item {
    String name, quantity, lastUpdated;

    public Item(String name, String qty, String date) {
        this.name = name;
        this.quantity = qty;
        this.lastUpdated = date;
    }
}
