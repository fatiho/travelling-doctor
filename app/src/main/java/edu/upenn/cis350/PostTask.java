package edu.upenn.cis350;

import android.os.AsyncTask;
import android.os.Build;
import android.util.Log;

import androidx.annotation.RequiresApi;

import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.ByteArrayInputStream;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.net.HttpURLConnection;
import java.net.URL;
import java.nio.charset.StandardCharsets;
import java.util.Scanner;

@RequiresApi(api = Build.VERSION_CODES.KITKAT)
public class PostTask extends AsyncTask<URL, String, String> { /*
This method is called in background when this object's "execute" method is invoked.
The arguments passed to "execute" are passed to this method.
*/
    String str;
    InputStream stream;
    PostTask(String s) {
        str = s;
        stream = new ByteArrayInputStream(s.getBytes(StandardCharsets.UTF_8));
    }
    @RequiresApi(api = Build.VERSION_CODES.KITKAT)
    protected String doInBackground(URL... urls) {
        try {
// get the first URL from the array
            URL url = urls[0];

            HttpURLConnection conn = (HttpURLConnection)url.openConnection();
            conn.setReadTimeout(10000);
            conn.setConnectTimeout(15000);
            conn.setRequestMethod("POST");
            conn.setRequestProperty("Content-Type", "application/json;charset=utf-8");
            conn.setRequestProperty("Accept", "application/json");
            conn.setDoOutput(true);
            String jsonInputString = str;
            try (OutputStream os = conn.getOutputStream()) {
                byte[] input = jsonInputString.getBytes("utf-8");
                os.write(input, 0, input.length);
            }
            catch (Exception e) {
                Log.e("error", "os", e);
            }
            try(BufferedReader br = new BufferedReader(
                    new InputStreamReader(conn.getInputStream(), "utf-8"))) {
                StringBuilder response = new StringBuilder();
                String responseLine = null;
                while ((responseLine = br.readLine()) != null) {
                    response.append(responseLine.trim());
                }
                System.out.println(response.toString());
                Log.d("stuck", "br");
            }
            catch (Exception e) {
                Log.e("error", "br", e);
            }
            conn.connect();
            return str;
        }
        catch (Exception e) {
            return e.toString();
        }
    }
    /*
    This method is called in foreground after doInBackground finishes. It can access and update Views in user interface.
    */
    protected void onPostExecute(String msg) {
// not implemented but you can use this if you’d like
    }
}