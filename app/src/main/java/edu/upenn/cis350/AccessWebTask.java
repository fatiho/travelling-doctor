package edu.upenn.cis350;

import android.os.AsyncTask;
import android.util.Log;

import org.json.JSONArray;
import org.json.JSONObject;

import java.net.HttpURLConnection;
import java.net.URL;
import java.util.Scanner;

public class AccessWebTask extends AsyncTask<URL, Void, JSONObject> { /*
This method is called in background when this object's "execute" method is invoked.
The arguments passed to "execute" are passed to this method.
*/
    protected JSONObject doInBackground(URL... urls) {
        try {
            // get the first URL from the array
            URL url = urls[0];
            // create connection and send HTTP request

            HttpURLConnection conn = (HttpURLConnection)url.openConnection();
            conn.setRequestMethod("GET");
            conn.connect();
            // read first line of data that is returned
            Scanner in = new Scanner(url.openStream());
            String msg = in.nextLine();
            // use Android JSON library to parse JSON
            Log.d("Whoopsies:", "I can't go on");
            JSONObject joe = new JSONObject(msg);
            //JSONObject jo = joe.getJSONObject(0);
            //Doctor one = new Doctor(jo.get("name").toString(), "dasd", "asda", "123", "asd", jo.get("age").toString());
            Log.d("I MADE IT:", "MADEEE IT!!");

            // assumes that JSON object contains a "name" field
            return joe;

            // this will be passed to onPostExecute method
            //return name;
        }
        catch (Exception e) {
            Log.d("EXCEPTION", e.toString());
            //return e.toString();
        }
        return new JSONObject();
    }
    /*
    This method is called in foreground after doInBackground finishes. It can access and update Views in user interface.
    */
    protected void onPostExecute(String msg) {
// not implemented but you can use this if you’d like
    }
}