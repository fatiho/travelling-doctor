package edu.upenn.cis350;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.os.Bundle;
import android.text.InputType;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class AppointmentsActivity extends AppCompatActivity {
    Map<String, String> appointments;
    Map<String, String> appointmentNotes;
    private String notes;
    private String getter;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        appointments = new HashMap<String, String>();
        appointments.put("jack", "pending");
        appointments.put("penny", "accepted");
        appointments.put("jill", "accepted");
        appointments.put("mommy", "pending");
        appointments.put("daddy", "accepted");
        appointments.put("baby", "pending");
        appointmentNotes = new HashMap<String, String>();
        appointmentNotes.put("jack", " ");
        appointmentNotes.put("penny", " ");
        appointmentNotes.put("jill", " ");
        appointmentNotes.put("mommy", " ");
        appointmentNotes.put("daddy", " ");
        appointmentNotes.put("baby", " ");
        Toast.makeText(this, "I can't do it man!", Toast.LENGTH_SHORT).show();
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_doctor_appoitments);

//        String json = loadJSONFromAsset("appointmentData.json");
//        try {
//            final JSONObject obj = new JSONObject(json);
//            final JSONArray docs = (JSONArray) obj.getJSONArray("appointments");
//            final JSONObject obj2 = new JSONObject(json);
//            obj2.put("name", "jack string");
//            obj2.put("status", "pending");
//            docs.put(obj2);
//
//
//            try {
//                FileWriter file = new FileWriter("doctorData.json");
//                file.write(docs.toString());
//                file.flush();
//                file.close();
//            } catch (IOException e) {
//                e.printStackTrace();
//            }
//
//        } catch (JSONException e) {
//            //e.printStackTrace();
//
//        }
//
//        String json2 = loadJSONFromAsset("appointmentData.json");
//        try {
//            final JSONObject obj = new JSONObject(json2);
//            final JSONArray docs = (JSONArray) obj.getJSONArray("appointments");
//            final JSONObject obj2 = new JSONObject(json2);
//            obj2.put("name", "Max Strong");
//            obj2.put("status", "accepted");
//            docs.put(obj2);
//
//
//            try {
//                FileWriter file = new FileWriter("doctorData.json");
//                file.write(docs.toString());
//                file.flush();
//                file.close();
//            } catch (IOException e) {
//                e.printStackTrace();
//            }
//
//        } catch (JSONException e) {
//            //e.printStackTrace();
//
//        }


        updateSpinner();
    }

//    @Override
//    public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
//        // On selecting a spinner item
//        String item = parent.getItemAtPosition(position).toString();
//
//        // Showing selected spinner item
//        Toast.makeText(parent.getContext(), "Selected: " + item, Toast.LENGTH_LONG).show();
//    }
    public void onNothingSelected(AdapterView<?> arg0) {
        // TODO Auto-generated method stub
    }

    public void updateSpinner() {
        // Spinner element
        Spinner upcomingSpinner = (Spinner) findViewById(R.id.upcoming_spinner);
        Spinner pendingSpinner = (Spinner) findViewById(R.id.pending_spinner);
        Spinner pastSpinner = (Spinner) findViewById(R.id.past_spinner);

        // Spinner click listener
        //upcomingSpinner.setOnItemSelectedListener((AdapterView.OnItemSelectedListener) this);
        //pendingSpinner.setOnItemSelectedListener((AdapterView.OnItemSelectedListener) this);

        // Spinner Drop down elements
        List<String> pendingAppointments = new ArrayList<String>();
        List<String> upcomingAppointment = new ArrayList<String>();
        List<String> pastAppointment = new ArrayList<String>();

        pendingAppointments.add(" ");
        upcomingAppointment.add(" ");
        pastAppointment.add(" ");
        for (Map.Entry<String, String> one : appointments.entrySet()) {
            if (one.getValue().equals("pending")) {
                pendingAppointments.add(one.getKey());
            } else if (one.getValue().equals("accepted")) {
                upcomingAppointment.add(one.getKey());
            } else if (one.getValue().equals("completed")) {
                pastAppointment.add(one.getKey());
            }
        }



        // Creating adapter for spinner
        ArrayAdapter<String> dataAdapterU = new ArrayAdapter<String>(this, android.R.layout.simple_spinner_item, upcomingAppointment);
        ArrayAdapter<String> dataAdapterP = new ArrayAdapter<String>(this, android.R.layout.simple_spinner_item, pendingAppointments);
        ArrayAdapter<String> dataAdapterC = new ArrayAdapter<String>(this, android.R.layout.simple_spinner_item, pastAppointment);

        // Drop down layout style - list view with radio button
        dataAdapterU.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        dataAdapterP.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        dataAdapterC.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);

        // attaching data adapter to spinner
        pastSpinner.setAdapter(dataAdapterC);
        upcomingSpinner.setAdapter(dataAdapterU);
        pendingSpinner.setAdapter(dataAdapterP);
    }




//    public String loadJSONFromAsset(String filename) {
//        String json = null;
//        try {
//            InputStream is = this.getAssets().open(filename);
//            int size = is.available();
//            byte[] buffer = new byte[size];
//            is.read(buffer);
//            is.close();
//            json = new String(buffer, "UTF-8");
//        } catch (IOException ex) {
//            ex.printStackTrace();
//            return null;
//        }
//        return json;
//    }

    public void onAcceptButtonClick(View v) {
        //Intent i = new Intent(this, InventoryActivity.class);
        Spinner spinner = (Spinner) findViewById(R.id.pending_spinner);
        String level = spinner.getSelectedItem().toString();
        if (!level.equals(" ")) {
            appointments.put(level, "accepted");
            updateSpinner();
        }
        //i.putExtra("MESSAGE", "HI");
        //startActivityForResult(i, 1);
    }

    public void onRejectButtonClick(View v) {
//        Intent i = new Intent(this, AppointmentsActivity.class);
//        i.putExtra("MESSAGE", "HI");
//        startActivityForResult(i, 1);

        Spinner spinner = findViewById(R.id.pending_spinner);
        String level = spinner.getSelectedItem().toString();
        if (!level.equals(" ")) {
            appointments.put(level, "rejected");
            updateSpinner();
        }
    }

    // Code attributed in part to: https://stackoverflow.com/questions/10903754/input-text-dialog-android
    public void onEditButtonClickPast(View v) {
        Spinner spinner = findViewById(R.id.past_spinner);
        String level = spinner.getSelectedItem().toString();
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setTitle("Add your notes for your appointment with " + level);

        // Set up the input
        final EditText input = new EditText(this);
        // Specify the type of input expected; this, for example, sets the input as a password, and will mask the text
        input.setInputType(InputType.TYPE_CLASS_TEXT /*| InputType.TYPE_TEXT_VARIATION_PASSWORD*/);
        builder.setView(input);

        getter = level;
        // Set up the buttons
        builder.setPositiveButton("OK", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                notes = input.getText().toString();
                appointmentNotes.put(getter, notes);
            }
        });

        builder.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.cancel();
            }
        });


        builder.show();
        //Intent i = new Intent(this, EditAppointment.class);
        //i.putExtra("MESSAGE", "HI");
        //startActivityForResult(i, 1);


    }

    public void onEditButtonClickUpcoming(View v) {
        Spinner spinner = findViewById(R.id.upcoming_spinner);
        //Intent i = new Intent(this, EditAppointment.class);
        //i.putExtra("MESSAGE", "HI");
        //startActivityForResult(i, 1);
        String level = spinner.getSelectedItem().toString();
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setTitle("Add your notes for your appointment with " + level);

        // Set up the input
        final EditText input = new EditText(this);
        // Specify the type of input expected; this, for example, sets the input as a password, and will mask the text
        input.setInputType(InputType.TYPE_CLASS_TEXT /*| InputType.TYPE_TEXT_VARIATION_PASSWORD*/);
        builder.setView(input);
        getter = level;
        // Set up the buttons
        builder.setPositiveButton("OK", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                notes = input.getText().toString();
                appointmentNotes.put(getter, notes);
            }
        });

        builder.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.cancel();
            }
        });

        builder.show();
        //Intent i = new Intent(this, EditAppointment.class);
        //i.putExtra("MESSAGE", "HI");
        //startActivityForResult(i, 1);




    }

    public void onEditButtonClickPending(View v) {
        Spinner spinner = findViewById(R.id.pending_spinner);
        //Intent i = new Intent(this, EditAppointment.class);
        //i.putExtra("MESSAGE", "HI");
        //startActivityForResult(i, 1);
        String level = spinner.getSelectedItem().toString();
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setTitle("Add your notes for your appointment with " + level);

        // Set up the input
        final EditText input = new EditText(this);
        // Specify the type of input expected; this, for example, sets the input as a password, and will mask the text
        input.setInputType(InputType.TYPE_CLASS_TEXT /*| InputType.TYPE_TEXT_VARIATION_PASSWORD*/);
        builder.setView(input);
        getter = level;
        // Set up the buttons
        builder.setPositiveButton("OK", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                notes = input.getText().toString();
                appointmentNotes.put(getter, notes);

            }
        });

        builder.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.cancel();
            }
        });

        builder.show();
        //Intent i = new Intent(this, EditAppointment.class);
        //i.putExtra("MESSAGE", "HI");
        //startActivityForResult(i, 1);




    }

    public void onCompletedButtonClick(View v) {
        Spinner spinner = findViewById(R.id.upcoming_spinner);
        String level = spinner.getSelectedItem().toString();
        appointments.put(level, "completed");
        updateSpinner();
            AlertDialog.Builder builder
                    = new AlertDialog
                    .Builder(AppointmentsActivity.this);
            builder.setMessage("Please remember to update the inventory and/or add notes!");
            builder.setTitle("Alert !");
            builder.setCancelable(false);
            builder
                    .setPositiveButton(
                            "Return to Appointments",
                            new DialogInterface
                                    .OnClickListener() {
                                @Override
                                public void onClick(DialogInterface dialog,
                                                    int which)
                                {
                                    dialog.dismiss();
                                }
                            });
            AlertDialog alertDialog = builder.create();
            alertDialog.show();



    }

    public void onViewButtonClickPending(View v) {
        Spinner spinner = findViewById(R.id.pending_spinner);
        String level = spinner.getSelectedItem().toString();
        AlertDialog.Builder builder
                = new AlertDialog
                .Builder(AppointmentsActivity.this);
        Log.d("Note:", " A");
        String show = appointmentNotes.get(level);
        Log.d("Note:", " AA");
        if (show.equals(" ")) {
            show = "No notes available for this patient";
        }
        Log.d("Note:", " AAA");
        builder.setMessage(show);
        Log.d("Note:", " AAAA");
        builder.setTitle("Appointment Notes for " + level);
        Log.d("Note:", " AAAAA");
        builder.setCancelable(false);
        Log.d("Note:", " AAAAAA");
        builder
                .setPositiveButton(
                        "Return to Appointments",
                        new DialogInterface
                                .OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog,
                                                int which)
                            {
                                dialog.dismiss();
                            }
                        });
        AlertDialog alertDialog = builder.create();
        alertDialog.show();



    }

    public void onViewButtonClickUpcoming(View v) {
        Spinner spinner = findViewById(R.id.upcoming_spinner);
        String level = spinner.getSelectedItem().toString();
        AlertDialog.Builder builder
                = new AlertDialog
                .Builder(AppointmentsActivity.this);
        String show = appointmentNotes.get(level);
        if (show.equals(" ")) {
            show = "No notes available for this patient";
        }
        builder.setMessage(show);
        builder.setTitle("Appointment Notes for " + level);
        builder.setCancelable(false);
        builder
                .setPositiveButton(
                        "Return to Appointments",
                        new DialogInterface
                                .OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog,
                                                int which)
                            {
                                dialog.dismiss();
                            }
                        });
        AlertDialog alertDialog = builder.create();
        alertDialog.show();

    }

    public void onViewButtonClickPast(View v) {
        Spinner spinner = findViewById(R.id.past_spinner);
        String level = spinner.getSelectedItem().toString();
        AlertDialog.Builder builder
                = new AlertDialog
                .Builder(AppointmentsActivity.this);
        String show = appointmentNotes.get(level);
        if (show.equals(" ")) {
            show = "No notes available for this patient";
        }
        builder.setMessage(show);
        builder.setTitle("Appointment Notes for " + level);
        builder.setCancelable(false);
        builder
                .setPositiveButton(
                        "Return to Appointments",
                        new DialogInterface
                                .OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog,
                                                int which)
                            {
                                dialog.dismiss();
                            }
                        });
        AlertDialog alertDialog = builder.create();
        alertDialog.show();
    }

}
