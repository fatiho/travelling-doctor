package edu.upenn.cis350;
import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;

public class AddItem extends AppCompatActivity {
    String[] months = {"January", "February", "March", "April", "May", "June", "July", "August",
            "September", "October", "November", "December"};

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.add_item_page);
    }

    public void onAddButtonClick(View view) {
        EditText name = findViewById(R.id.name);
        EditText quantity = findViewById(R.id.quantity);
        DatePicker datePicker = findViewById(R.id.datePicker);

        //Get item name and quantity:
        String itemName = name.getText().toString();
        String qty = quantity.getText().toString();

        //Get date:
        int day  = datePicker.getDayOfMonth();
        int month = datePicker.getMonth();
        int year = datePicker.getYear();
        String date = months[month] + " " + String.valueOf(day) + ", " + String.valueOf(year);

        String[] result = {itemName, qty, date};

        Intent returnIntent = new Intent();
        returnIntent.putExtra("Result", result);
        setResult(Activity.RESULT_OK, returnIntent);
        Toast.makeText(getApplicationContext(), "Item added to inventory!",
                Toast.LENGTH_SHORT).show();
        finish();

        Log.d("What was recorded", itemName + " " + qty + " " + date);
    }
}
