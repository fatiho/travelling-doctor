package edu.upenn.cis350;

import androidx.appcompat.app.AppCompatActivity;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStream;
import java.net.URL;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

public class patientActivity extends AppCompatActivity {

    EditText username,password,email,telephone,fullName;
    Button create_account;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_patient);
        username = (EditText) findViewById(R.id.editText2);
        password = (EditText) findViewById(R.id.editText3);
        email = (EditText) findViewById(R.id.editText5);
        telephone = (EditText) findViewById(R.id.editText6);
        fullName = (EditText) findViewById(R.id.editText4);

        create_account = (Button) findViewById(R.id.button);
        create_account.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                // write to Json file
                // go to patient homepage

                // String json = loadJSONFromAsset("patientData.json");
//                try {
//                    final JSONObject obj = new JSONObject(json);
//                    final JSONArray docs = (JSONArray) obj.getJSONArray("doctors");
//                    final JSONObject obj2 = new JSONObject(json);
//                    obj2.put("username", username.getText().toString());
//                    obj2.put("password", password.getText().toString());
//                    obj2.put("telephone", telephone.getText().toString());
//                    obj2.put("email", email.getText().toString());
//                    obj2.put("fullname", fullName.getText().toString());
//
//                    docs.put(obj2);
                    String msg = "";
                    try {
                        URL url = new URL("http://10.0.2.2:3000/createpatient?username=" + username.getText().toString()
                                + "&password=" + password.getText().toString() + "&telephone=" + telephone.getText().toString()
                                + "&email=" + email.getText().toString() + "&fullname=" + fullName.getText().toString());
                        CreateUserOnline task = new CreateUserOnline();
                        task.execute(url);
                        msg = task.get();
                        Log.d("PRINTING NAME", "HI!!!!!");
                        Context context = getApplicationContext();
                        CharSequence text = "Hello toast!";
                        int duration = Toast.LENGTH_SHORT;

                        Toast toast = Toast.makeText(context, text, duration);
                        toast.show();


                    }
                    catch (Exception e) {
                        //return e.toString();
                    }


//                    try {
//                        FileWriter file = new FileWriter("patientData.json");
//                        file.write(docs.toString());
//                        file.flush();
//                        file.close();
//                    } catch (IOException e) {
//                        e.printStackTrace();
//                    }
//
//                } catch (JSONException e) {
//                    //e.printStackTrace();
//
//                }



               Intent i = new Intent(patientActivity.this, PatientHomepageActivity.class);
                i.putExtra("username",username.getText().toString() );
                i.putExtra("password",password.getText().toString() );
                i.putExtra("telephone", telephone.getText().toString());
                i.putExtra("email", email.getText().toString());
                i.putExtra("fullName", fullName.getText().toString());


                startActivity(i);





            }
        });

    }

    public String loadJSONFromAsset(String filename) {
        String json = null;
        try {
            InputStream is = this.getAssets().open(filename);
            int size = is.available();
            byte[] buffer = new byte[size];
            is.read(buffer);
            is.close();
            json = new String(buffer, "UTF-8");
        } catch (IOException ex) {
            ex.printStackTrace();
            return null;
        }
        return json;
    }





}
