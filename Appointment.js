var mongoose = require('mongoose');

// note: your host/port number may be different!
mongoose.connect('mongodb+srv://nabeel:nabeel@fanyhealth-mek3v.mongodb.net/test?retryWrites=true&w=majority');

var Schema = mongoose.Schema;

var appointmentSchema = new Schema( {
      d_username: String,
      p_username: String,
      status: String,
      p_telephone: String,
      need: String,
      address: String,
      date_time: String
    } );


module.exports = mongoose.model('Appointment', appointmentSchema);