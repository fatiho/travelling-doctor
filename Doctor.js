var mongoose = require('mongoose');

// note: your host/port number may be different!
mongoose.connect('mongodb+srv://nabeel:nabeel@fanyhealth-mek3v.mongodb.net/test?retryWrites=true&w=majority');

var Schema = mongoose.Schema;

var doctorSchema = new Schema( {
      username: {type: String, required: true, unique: true},
      password: {type: String, required: true},
      email: {type: String, required: true},
      telephone: {type: String, required: true},
      speciality: {type: String, required: true},
      rate: {type: String, required: true}
    } );


module.exports = mongoose.model('Doctor', doctorSchema);

