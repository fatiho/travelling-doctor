# README #

# Welcome to the Traveling Doctor App
I worked with a team in the Spring of 2020 to design an app for patients to request a doctor to visit them. A doctor could then use the app to accept or reject the appointment. Users will have the option to sign up and then log in after using the app.

This app was a major milestone for me. It was the first mobile app I had developed. 

The development process utilized Android Studio, Java, MongoDB and NodeJS.

## How do I get set up? 

* Launch Android Studio. The project folder is in `src/main`
* Run `node index.js` fromm the root folder.