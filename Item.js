var mongoose = require('mongoose');

// the host:port must match the location where you are running MongoDB
// the "myDatabase" part can be anything you like
mongoose.connect('mongodb+srv://nabeel:nabeel@fanyhealth-mek3v.mongodb.net/test?retryWrites=true&w=majority');

var Schema = mongoose.Schema;

var itemScheme = new Schema({
    name: {type: String, required: true},
    quantity: {type: Number, min: 0},
    lastUpdated: {type: Date, default: Date.now},
});

itemScheme.methods.edit = function () {
    this.lastUpdated = new Date();
}

// export personSchema as a class called Person
module.exports = mongoose.model('InventoryItem', itemScheme);
