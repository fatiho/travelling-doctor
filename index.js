// set up Express
var express = require('express');
var app = express();

// set up EJS
app.set('view engine', 'ejs');

// set up BodyParser
var bodyParser = require('body-parser');
app.use(bodyParser.urlencoded({ extended: true }));

// import the Person class from Person.js
var Person = require('./Person.js');

// import the inventory class
var Item = require('./Item.js')

// import the doctor class
var Doctor = require('./Doctor.js')

// import the patient class
var Patient = require('./Patient.js')

// import the appointment class
var Appointment = require('./appointment.js')

/***************************************/
// creating a new item
app.post('/createItem', (req, res) => {
	var newItem = new Item ({
		name: req.body.name,
		quantity: req.body.quantity,
		lastUpdated: req.body.date,
	});

	newItem.save((err) => {
		if (err) {
			console.log(err);
			res.end();
			
		} else {
			
		}
	});
})

// route for creating a new appointment
app.post('/createapp', (req, res) => {
	var newAppointment = new Appointment ({
		d_username: req.body.d_username,
		p_username: req.body.p_username,
		status: req.body.status,
		p_telephone: req.body.p_telephone,
		need: req.body.need,
		address: req.body.address,
		date_time: req.body.date_time
    	});

	// save the person to the database
	newAppointment.save( (err) => { 
		if (err) {
    			res.type('html').status(200);
    			res.write('uh oh: ' + err);
    			console.log(err);
    			res.end();
		}
		else {
    			// display the "successfull created" page using EJS
				res.write('new app', { app : newAppointment });
		}
    	} ); 
    }
);

// route for showing all the people
app.use('/allapps', (req, res) => {
    
	// find all the Person objects in the database
	Appointment.find( {}, (err, apps) => {
	if (err) {
    		res.type('html').status(200);
    		console.log('uh oh' + err);
    		res.write(err);
	}
	else {
    		if (apps.length == 0) {
			res.type('html').status(200);
			res.write('There are no appointments');
			res.end();
			return;
    		}
    		// use EJS to show all the people
    		res.render('all', { apps: apps });

	}
    }); 
});


// route for creating a new person
// this is the action of the "create new person" form
app.use('/create', (req, res) => {
	// construct the Person from the form data which is in the request body
	var newPerson = new Person ({
		username: req.query.name,
		password: req.query.age,
		email: req.query.email,
		telephone: req.query.telephone,
		specialty: req.query.specialty,
		rate: req.query.rate
	    });

	// save the person to the database
	newPerson.save( (err) => { 
		if (err) {
		    res.type('html').status(200);
		    res.write('uh oh: ' + err);
		    console.log(err);
		    res.end();
		}
		else {
		    // display the "successfull created" page using EJS
		    res.write('ok');
		}
	    } ); 
    }
    );

// route for creating a new patient
app.use('/createpatient', (req, res) => {
	var newPatient = new Patient ({
		username: req.query.username,
		password: req.query.password,
		email: req.query.email,
		telephone: req.query.telephone,
		fullname: req.query.specialty
	    });

	// save the person to the database
	newPatient.save( (err) => { 
		if (err) {
		    res.type('html').status(200);
		    res.write('uh oh: ' + err);
		    console.log(err);
		    res.end();
		}
		else {
		    // display the "successfull created" page using EJS
		    res.write('ok');
		}
	    } ); 
    }
    );

// route for showing all the people
app.use('/all', (req, res) => {
    
	// find all the Person objects in the database
	Person.find( {}, (err, persons) => {
		if (err) {
		    res.type('html').status(200);
		    console.log('uh oh' + err);
		    res.write(err);
		}
		else {
		    if (persons.length == 0) {
			res.type('html').status(200);
			res.write('There are no people');
			res.end();
			return;
		    }
		    // use EJS to show all the people
		    res.render('all', { persons: persons });

		}
	    }).sort({ 'age': 'asc' }); // this sorts them BEFORE rendering the results
    });

// route for accessing data via the web api
// to use this, make a request for /api to get an array of all Person objects
// or /api?name=[whatever] to get a single object
app.use('/apiPat', (req, res) => {
	console.log("LOOKING FOR SOMETHING?");

	// construct the query object
	var queryObject = {};
	if (req.query.username) {
	    // if there's a name in the query parameter, use it here
	    queryObject = { "username" : req.query.username };
	}
    
	Patient.find( queryObject, (err, patients) => {
		console.log(patients);
		if (err) {
		    console.log('uh oh' + err);
		    res.json({});
		}
		else if (patients.length == 0) {
		    // no objects found, so send back empty json
		    res.json({});
		}
		else if (patients.length == 1 ) {
		    var patient = patients[0];
		    // send back a single JSON object
		    res.json( { "username" : patient.name , "password" : patient.password,
				"email" : patient.email , "telephone" : patient.telephone,
				"fullname" : patient.fullname , "email" : patient.email } );
		}
		else {
		    // construct an array out of the result
		    var returnArray = [];
		    patients.forEach( (patient) => {
			    returnArray.push( { "username" : patient.name , "password" : patient.password,
				"email" : patient.email , "telephone" : patient.telephone,
				"fullname" : patient.fullname , "email" : patient.email }  );
			});
		    // send it back as JSON Array
		    res.json(returnArray); 
		}
		
	    });
    });


// route for accessing data via the web api
// to use this, make a request for /api to get an array of all Person objects
// or /api?name=[whatever] to get a single object
app.use('/apiDoc', (req, res) => {
	console.log("LOOKING FOR SOMETHING?");

	// construct the query object
	var queryObject = {};
	if (req.query.username) {
	    // if there's a name in the query parameter, use it here
	    queryObject = { "username" : req.query.username };
	}
    	console.log("MADE IT HERE");
	console.log(req.query.username);
	Doctor.find( queryObject, (err, doctors) => {
		console.log(doctors);
		if (err) {
		    console.log('uh oh' + err);
		    res.json({});
		}
		else if (doctors.length == 0) {
		    // no objects found, so send back empty json
		    res.json({});
		}
		else if (doctors.length == 1 ) {
		    var doctor = doctors[0];
		    // send back a single JSON object
		    res.json( { "username" : doctor.username , "password" : doctor.password,
				"email" : doctor.email , "telephone" : doctor.telephone,
				"specialty" : doctor.specialty , "rate" : doctor.rate } );
		}
		else {
		    // construct an array out of the result
		    var returnArray = [];
		    doctors.forEach( (doctor) => {
			    returnArray.push( { "username" : doctor.username , "password" : doctor.password,
				"email" : doctor.email , "telephone" : doctor.telephone,
				"specialty" : doctor.specialty , "rate" : doctor.rate }  );
			});
		    // send it back as JSON Array
		    res.json(returnArray); 
		}
		
	    });
    });





/*************************************************/

app.use('/public', express.static('public'));

app.use('/', (req, res) => { res.redirect('/public/personform.html'); } );

app.listen(3000,  () => {
	console.log('Listening on port 3000');
    });
